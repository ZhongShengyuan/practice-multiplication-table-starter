package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        Boolean isValid = isValid(start, end);
        if(isValid){
            String table = generateTable(start, end);
            return table;
        }
        return null;
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start, end);
    }

    public Boolean isInRange(int number) {
        return number >=1 && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end ? true : false;
    }

    public String generateTable(int start, int end) {
        StringBuffer sb = new StringBuffer();
        for(int i =start; i <= end; i++){
            sb.append(generateLine(start, i)).append("\r\n");
        }
        return sb.toString().trim();
    }

    public String generateLine(int start, int row) {
        StringBuffer sb = new StringBuffer();
        for( int i = start; i <=row; i++){
            sb.append(generateSingleExpression(i, row));
            sb.append("  ");
        }
        String line = sb.toString().trim();
        return line;
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        return multiplicand + "*" + multiplier + "=" + multiplicand*multiplier;
    }
}
